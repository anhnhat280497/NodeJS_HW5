const express = require('express');
const bodyParser = require('body-parser');
const homeapi = require('./apis/home.api');
const noteapi = require('./apis/note.api');
const userapi = require('./apis/user.api');


const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('view options', { layout: false });

app.get('/', homeapi);
app.get('/users', userapi.findAllUser);
app.get('/users/:id', userapi.findIdOfUsers);
app.post('/users', userapi.createUser);
app.put('/users/:id', userapi.updateUser);
app.delete('/users/:id', userapi.deleteUser);
app.get('/users/:id/notes', userapi.fileNotesOfUserController); // list notes of user

app.get('/notes', noteapi.findAllNotes);
app.get('/notes/:id', noteapi.findIdOfNotes);
app.post('/notes', noteapi.createNote);
app.put('/notes/:id', noteapi.updateNote);
app.delete('/notes/:id', noteapi.deleteNote);

app.get('/nhat', userapi.newUser);


const port = +process.env.PORT || 6060;

app.listen(port, (error) => {
  if (error) {
    return console.error('run server got an error', error);
  }
  return console.log(`Server listening ${port}`);
});
