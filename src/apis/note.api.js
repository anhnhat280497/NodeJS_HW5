
const noteRepo = require('../repository/note.repo');
const writeNote = require('../repository/writeFileNote');

const findAllNotes = async (req, res) => {
  try {
    const notes = await noteRepo.allNotes();
    res.json(notes);
  } catch (err) {
    res.json({ error: err.message });
  }
};

const findIdOfNotes = async (req, res) => {
  try {
    const notes = await noteRepo.allNotes();
    const noteIndex = await noteRepo.noteFindId(req.params.id);
    res.json(notes[noteIndex]);
  } catch (err) {
    res.json({ error: err.message });
  }
};

const createNote = async (req, res) => {
  try {
    const noteNew = {
      id: 'qwertyuiop54321',
      title: 'New Note',
      content: 'Nguyen Anh Nhat',
      createById: 'nananananan280497794082',
    };
    const notes = await noteRepo.allNotes(); // Đọc toàn bộ file note ra notes
    notes.unshift(noteNew); // Thêm cái note mới vào
    const notesNew = JSON.stringify(notes, null, 2); // chuyển về string
    writeNote.writeFile(notesNew);
    res.json(notes);
  } catch (err) {
    res.json({ error: err.message });
  }
};

const updateNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const noteIndex = noteRepo.noteFindId(noteId);
    if (noteIndex !== -1) {
      const noteUpdate = {
        id: noteId,
        title: 'Update Note',
        content: 'Update Nguyen Anh Nhat',
        createById: 'nananananan280497794082',
      };
      const notes = await noteRepo.allNotes();
      notes[noteIndex] = noteUpdate;
      const notesNew = JSON.stringify(notes, null, 2);
      writeNote.writeFile(notesNew);
      res.json(notes);
    } else { res.send('Not found NoteId'); }
  } catch (err) {
    res.json({ error: err.message });
  }
};

const deleteNote = async (req, res) => {
  try {
    const noteId = req.params.id;
    const noteIndex = noteRepo.noteFindId(noteId);
    if (noteIndex !== -1) {
      const notes = await noteRepo.allNotes();
      notes.splice(noteIndex, 1);
      const notesNew = JSON.stringify(notes, null, 2);
      writeNote.writeFile(notesNew);
      res.json(notes);
      // res.sendStatus(204);
    } else { res.send('Not found NoteId'); }
  } catch (err) {
    res.json({ error: err.message });
  }
};

// export to other modules
module.exports = {
  findAllNotes,
  findIdOfNotes,
  createNote,
  updateNote,
  deleteNote,
};
