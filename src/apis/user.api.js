const fs = require('fs');
const path = require('path');
const utils = require('../utils');
const userRepo = require('../repository/user.repo');
const noteRepo = require('../repository/note.repo');

const findAllUser = async (req, res) => {
  try {
    const users = await userRepo.allUsers();
    res.json(users);
  } catch (err) {
    res.json({ error: err.message });
  }
};

const findIdOfUsers = async (req, res) => {
  try {
    const users = await userRepo.allUsers();
    const userIndex = await userRepo.userFindId(req.params.id);    
    res.json(users[userIndex]);
  } catch (err) {
    res.json({ error: err.message });
  }
};

const createUser = async (req, res) => {
  try {
    const userForm = req.body;
    userForm.id = 'asdfghjkl12345';
    res.json(userForm);
  } catch (err) {
    res.json({ error: err.message });
  }
};

const updateUser = (req, res) => {
  try {
    const userId = req.params.id;
    console.log(req.body);
    console.log(userId);
    res.json({ update: 'ok' });
  } catch (err) {
    res.json({ error: err.message });
  }
};

const deleteUser = (req, res) => {
  try {
    const userId = req.params.id;
    console.log('Delete User: ', userId);
    res.sendStatus(204);
  } catch (err) {
    res.json({ error: err.message });
  }
};


const fileNotesOfUserController = async (req, res) => {
  try {
    const users = await userRepo.allUsers();
    const user = await userRepo.userFindId(req.params.id);
    if (user !== -1) {
      const listNotesOfUser = await noteRepo.notesFindCreateById(users[user].id);
      users[user].notes = listNotesOfUser;
      res.json(users[user]);
    }
  } catch (err) {
    res.json({ error: err.message });
  }
};

const newUser = async (req, res) => {
  try {
    const student = {
      id: 'qwqrete223535', firstName: 'Nhat', lastName: 'Nguyen', dateOfBirth: '1997-04-28',
    };
    const linkFile = path.join(utils.rootPath, 'data', 'newUser.json');
    const userS = await userRepo.allUsers();
    userS.push(student);
    const data2 = JSON.stringify(userS, null, 2);
    console.log(data2);
    fs.writeFileSync(linkFile, data2);
    res.json(userS);
  } catch (err) {
    res.json({ error: err.message });
  }
};

module.exports = {
  findAllUser,
  findIdOfUsers,
  createUser,
  updateUser,
  deleteUser,
  fileNotesOfUserController,
  newUser,
};
