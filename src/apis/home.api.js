const path = require('path');
const utils = require('../utils');


const index = async (req, res) => {
  const indexFile = path.join(utils.rootPath, 'template', 'index.html');
  try {
    const indexContent = await utils.readFile(indexFile);
    res.header('Content-Type', 'text/html');
    res.send(indexContent);
  } catch (error) {
    res.json({ error: error.message });
  }
};

module.exports = index;
