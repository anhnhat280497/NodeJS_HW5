const path = require('path');
const utils = require('../utils');

class NoteRepo {
  async allNotes() {
    const notesFile = path.join(utils.rootPath, 'data', 'notes.json');
    const notesContent = await utils.readFile(notesFile);
    const notesJson = JSON.parse(notesContent);
    return notesJson;
  }

  async noteFindId(noteId) {
    const notes = await this.allNotes();
    const noteDetail = notes.findIndex(note => note.id === noteId);
    return noteDetail;
  }

  async notesFindCreateById(userId) {
    const notes = await this.allNotes();
    const listNotesOfUser = notes.filter(note => note.createById === userId);
    return listNotesOfUser;
  }
}

const noteRepo = new NoteRepo();

module.exports = noteRepo;
