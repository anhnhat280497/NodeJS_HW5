const path = require('path');
const utils = require('../utils');

class UserRepo {
  async allUsers() {
    const userFile = path.join(utils.rootPath, 'data', 'users.json');
    const userContent = await utils.readFile(userFile);
    const userJson = JSON.parse(userContent);
    return userJson;
  }

  async userFindId(userId) {
    const users = await this.allUsers();
    const usersDetail = users.findIndex(user => user.id === userId);
    return usersDetail;
  }
}

const userRepo = new UserRepo();

module.exports = userRepo;
