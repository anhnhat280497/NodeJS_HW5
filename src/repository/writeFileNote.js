const path = require('path');
const fs = require('fs');
const utils = require('../utils');

class WriteNote {
  async writeFile(notesNew) {
    const newNoteFile = path.join(utils.rootPath, 'data', 'newNote.json'); // đường dẫn
    await fs.writeFileSync(newNoteFile, notesNew); // ghi file
  }
}

const writeNote = new WriteNote();

module.exports = writeNote;
